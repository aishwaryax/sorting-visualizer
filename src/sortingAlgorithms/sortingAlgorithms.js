//merge sort

export function mergeSort(array) {
    const animations = []
    if (array.length <= 1) return array;
    const auxilaryArray = array.slice()
    mergeSortHelper(array, 0, array.length - 1, auxilaryArray, animations)
    return animations
}

function mergeSortHelper(
    mainArray,
    initial,
    final,
    auxilaryArray,
    animations
) {
    if (initial === final) return
    const mid = Math.floor((initial + final) / 2)
    mergeSortHelper(auxilaryArray, initial, mid, mainArray, animations)
    mergeSortHelper(auxilaryArray, mid + 1, final, mainArray, animations)
    doMerge(mainArray, initial, mid, final, auxilaryArray, animations)

}

function doMerge(
    mainArray,
    initial,
    mid,
    final,
    auxilaryArray,
    animations) {
    let k = initial,
        i = initial,
        j = mid + 1
    while (i <= mid && j <= final) {
        animations.push([i, j]) //comparision change color
        animations.push([i, j]) //revert color
        if (auxilaryArray[i] <= auxilaryArray[j]) {
            animations.push([k, auxilaryArray[i]])
            mainArray[k++] = auxilaryArray[i++]
        } else {
            animations.push([k, auxilaryArray[j]])
            mainArray[k++] = auxilaryArray[j++]
        }
    }
    while (i <= mid) {
        animations.push([i, i]) //comparision change color
        animations.push([i, i]) //revert color
        animations.push([k, auxilaryArray[i]])
        mainArray[k++] = auxilaryArray[i++]
    }
    while (j <= final) {
        animations.push([j, j]) //comparision change color
        animations.push([j, j]) //revert color
        animations.push([k, auxilaryArray[j]])
        mainArray[k++] = auxilaryArray[j++]
    }

}

//bubblesort

export function bubbleSort(array) {
    const animations = []
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length - 1 - i; j++) {
            animations.push([j + 1, j])
            var flag = false
            if (array[j + 1] < array[j]) {
                [array[j + 1], array[j]] = [array[j], array[j + 1]]
                flag = true //swapped
            }
            animations.push([j + 1, j, flag])
            animations.push([j + 1, j])
        }
    }
    return animations
}

export function naiveBubbleSort(array) {
    const animations = []
    for (let i = 0; i < array.length; i++) {
        for (let j = 0; j < array.length - 1; j++) {
            animations.push([j + 1, j])
            var flag = false
            if (array[j + 1] < array[j]) {
                [array[j + 1], array[j]] = [array[j], array[j + 1]]
                flag = true //swapped
            }
            animations.push([j + 1, j, flag])
            animations.push([j + 1, j])
        }
    }
    return animations
}

export function insertionSort(array) {
    var anims = [];
    for (let i = 0; i < array.length; i++) {
        var value = array[i];
        var hole = i;
        while (hole > 0 && array[hole - 1] > value) {
            anims.push(["M", hole, hole - 1]);
            anims.push(["A", hole, hole - 1]);
            anims.push(["U", hole, hole - 1]);
            array[hole] = array[--hole];
        }
        array[hole] = value;
        anims.push(["FA", hole, value])
    }
    return anims;
}

export function selectionSort(array) {
    var anims = [];
    for (let i = 0; i < array.length; i++) {
        var minidx = i;
        anims.push(["Change", minidx, minidx]);
        anims.push(["Start", i]);
        for (let j = i + 1; j < array.length; j++) {
            anims.push(["Traverse", j]);
            if (array[j] < array[minidx]) {
                anims.push(["Change", minidx, j]);
                if (minidx === i) anims.push(["Start", i]);
                minidx = j;
            } else {
                anims.push(["Retraverse", j]);
            }

        }
        [array[i], array[minidx]] = [array[minidx], array[i]];
        anims.push(["Swap", minidx, i]);
        anims.push(["End", i]);
    }
    console.log(array);
    return anims;


}