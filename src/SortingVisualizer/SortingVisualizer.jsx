import React, { Component } from 'react'
import './SortingVisualizer.css'
import { Navbar, Button, Nav } from 'react-bootstrap'
import { mergeSort, bubbleSort, naiveBubbleSort, insertionSort, selectionSort } from '../sortingAlgorithms/sortingAlgorithms'
const MAX_WIDTH = 5
const MIN_WIDTH = 2
const MAX_N = window.innerHeight - window.innerHeight/2
const MIN_N = 20
const MAX_SPEED = 25
const MIN_SPEED = 1
const MAX_MARGIN = 5
const MIN_MARGIN = 1
export default class SortingVisualizer extends Component {
    constructor(props) {
        super(props)

        this.state = {
            array: [],
            width: 2,
            size: 100,
            speedInput: 50,
            arraySize: MAX_N,
            sorting: false,
            margin: MIN_MARGIN         
        }
      
        this.handleEvent = this.handleEvent.bind(this)
    }

    componentDidMount() {
        this.resetArray()        
    }

    resetArray() {
        var array = []
        var {width, size} = this.state
        if(parseInt(size)===0){
             this.setState({arraySize: MIN_N})
        }
        else{
            this.setState({arraySize: Math.floor(MAX_N*parseInt(size)/100)})
        }
        var upperLimit = Math.floor(window.innerHeight) - 60
        width = MAX_WIDTH - size/100 * (MAX_WIDTH - MIN_WIDTH)
        var margin = MAX_MARGIN - size/100 * (MAX_MARGIN - MIN_MARGIN)
        for (let i=0; i<this.state.arraySize; i++) {
            array.push(randomIntFromInterval(5, upperLimit))
        }
        this.setState({array, width, margin})
        this.enable()

    }

    componentDidUpdate(prevProps, prevState, snapshot) { if (prevState.name !== this.state.name) { this.handler() } }

    componentWillUnmount() {
        
    }

    // Prototype methods, Bind in Constructor (ES2015)
    handleEvent() {}

    // Class Properties (Stage 3 Proposal)
    handler = () => { this.setState() }

    disable(){
        const elementsToBeDisabled = [...document.getElementsByClassName("slider"), ...document.getElementsByClassName("button")]
        for(let i=0; i<elementsToBeDisabled.length; i++) elementsToBeDisabled[i].disabled = true    
    }
    enable(){
        const elementsToBeDisabled = [...document.getElementsByClassName("slider"), ...document.getElementsByClassName("button")]
        for(let i=0; i<elementsToBeDisabled.length; i++) elementsToBeDisabled[i].disabled = false  
    }

    

    bubbleSort(){
        this.disable()
        const animations = bubbleSort(this.state.array)
        var speed = MAX_SPEED - parseInt(this.state.speedInput)/100*(MAX_SPEED - MIN_SPEED)
        for(let i = 0; i < animations.length; i++){
            const arrayBars = document.getElementsByClassName('array-bar')
            if(i%3 !== 1) {
                const [barOneIndex, barTwoIndex] = animations[i]
                const barOneStyle = arrayBars[barOneIndex].style
                const  barTwoStyle = arrayBars[barTwoIndex].style
                const color = i % 3 === 0 ? 'red' : '#314ee4'
                setTimeout(() => {
                    barOneStyle.backgroundColor = color
                    barTwoStyle.backgroundColor = color
                }, i * speed)
            } else {
                const [barOneIndex, barTwoIndex, flag] = animations[i]
                const barOneStyle = arrayBars[barOneIndex].style
                const  barTwoStyle = arrayBars[barTwoIndex].style
                if (flag)
                setTimeout(() => {
                    [barOneStyle.height, barTwoStyle.height] = [barTwoStyle.height, barOneStyle.height]
                }, i * speed)
            }
        }
    }

    naiveBubbleSort(){
        this.disable()
        const animations = naiveBubbleSort(this.state.array)
        const arrayBars = document.getElementsByClassName('array-bar')
        var speed = MAX_SPEED - parseInt(this.state.speedInput)/100*(MAX_SPEED - MIN_SPEED)
        for(let i = 0; i < animations.length; i++){
            if(i%3 !== 1) {
                const [barOneIndex, barTwoIndex] = animations[i]
                const barOneStyle = arrayBars[barOneIndex].style
                const  barTwoStyle = arrayBars[barTwoIndex].style
                const color = i % 3 === 0 ? 'red' : '#314ee4'
                setTimeout(() => {
                    barOneStyle.backgroundColor = color
                    barTwoStyle.backgroundColor = color
                }, i * speed)
            } else {
                const [barOneIndex, barTwoIndex, flag] = animations[i]
                const barOneStyle = arrayBars[barOneIndex].style
                const  barTwoStyle = arrayBars[barTwoIndex].style
                if (flag)
                setTimeout(() => {
                    [barOneStyle.height, barTwoStyle.height] = [barTwoStyle.height, barOneStyle.height]
                }, i * speed)
            }
        }
    }

    insertionSort() {
        this.enable()
        const animations = insertionSort(this.state.array)
        var speed = MAX_SPEED - parseInt(this.state.speedInput)/100*(MAX_SPEED - MIN_SPEED)
        const arrayBars = document.getElementsByClassName('array-bar')
        for(let i = 0; i < animations.length; i++) {
            const [type, val1, val2] = animations[i];
            if (type === "M" || type === "U"){
            const color = type === "M" ? "red" : "#314ee4";
            const barOneStyle = arrayBars[val1].style
            const barTwoStyle = arrayBars[val2].style
            setTimeout(()=>{
                barOneStyle.backgroundColor = color
                barTwoStyle.backgroundColor = color
            },i*speed)
            }
            else if (type === "A"){
                const barOneStyle=arrayBars[val1].style
                const barTwoStyle = arrayBars[val2].style
                setTimeout(() => {
                    barOneStyle.height = barTwoStyle.height
                }, i * speed)
            }
            else {
                const barOneStyle = arrayBars[val1].style
                const value=val2
                setTimeout(() => {
                    barOneStyle.height=`${value}px`;    //Assign the height of the line to the specified value
                }, i * speed)
            }
        }
    }

    selectionSort(){
        this.disable()
        var speed = MAX_SPEED - parseInt(this.state.speedInput)/100*(MAX_SPEED - MIN_SPEED)
        const animations=selectionSort(this.state.array)
        var arrayBars=document.getElementsByClassName("array-bar")
        for(let i=0; i < animations.length; i++) {
            const type = animations[i][0]
            if (type === "Start") {
            const current = animations[i][1]
            const currentBarStyle = arrayBars[current].style;
            setTimeout(()=>{
              currentBarStyle.background="black"
            }, i * speed)
            }
            else if (type === "End"){
                const current = animations[i][1];
                const currentBarStyle = arrayBars[current].style;
                setTimeout(()=>{
                currentBarStyle.background = "#314ee4"
                },i*speed)
            } 
            else if (type === "Change") {
                const [prev, current]=[animations[i][1], animations[i][2]]
                const previousBarStyle=arrayBars[prev].style
                const currentBarStyle=arrayBars[current].style
                setTimeout(()=>{
                previousBarStyle.background="#314ee4"
                currentBarStyle.background="black"
                }, i * speed)
            }
            else if(type==="Traverse"){
                const index = animations[i][1]
                const currentBarStyle = arrayBars[index].style
                setTimeout(()=>{
                currentBarStyle.background="red" 
                }, i * speed)
            }
            else if(type==="Retraverse"){
                const [type,index]=animations[i];
                const currentBarStyle=arrayBars[index].style;
                setTimeout(()=>{
                currentBarStyle.background="#314ee4"
                },i * speed)
            }
            else if(type==="Swap"){
                const [ prev, current ]=[animations[i][1], animations[i][2]]
                const previousBarStyle = arrayBars[prev].style;
                const currentBarStyle = arrayBars[current].style;
                setTimeout(() => {  
                [previousBarStyle.height,currentBarStyle.height] = [currentBarStyle.height,previousBarStyle.height];
                [previousBarStyle.background, currentBarStyle.background]=["#314ee4","#314ee4"];
                }, i * speed)
            }
            }
      }
    

    mergeSort() {
        this.disable()
        const animations = mergeSort(this.state.array)
        var speed = MAX_SPEED - parseInt(this.state.speedInput)/100*(MAX_SPEED - MIN_SPEED)
        for (let i = 0; i < animations.length; i++) {
            const arrayBars = document.getElementsByClassName('array-bar')
            const isColorChange = i % 3 !== 2
            if (isColorChange) {
                const color = i % 3 === 0 ? 'red' : 'turquoise'
                const [barOneIndex, barTwoIndex] = animations[i]
                const barOneStyle = arrayBars[barOneIndex].style
                const  barTwoStyle = arrayBars[barTwoIndex].style
                setTimeout(() => {
                    barOneStyle.backgroundColor = color
                    barTwoStyle.backgroundColor = color
                }, i * speed)   
            }
            else {
                setTimeout(() => {
                    const [barOneIndex, newHeight] = animations[i]
                    const barOneStyle = arrayBars[barOneIndex].style
                    barOneStyle.height = `${newHeight}px`
                }, i * speed)
            }
        }
    }
    quickSort(){}


    handle = (name) => (event) => {
        this.setState({[name]: event.target.value})
        this.resetArray()
    }


    render() {
    
        return (
            <>
              <Navbar style={{background: "#070e33"}} variant="dark">
                <Navbar.Brand href="/">Sorting Visualizer</Navbar.Brand>
                <Nav className="mr-auto" >
                    <input
                        className="slider"
                        type="range"
                        min="0"
                        max="100"
                        onChange={this.handle("speedInput")}
                        value={this.state.speedInput}
                    />
                    <input
                        className="slider"
                        type="range"
                        min="0"
                        max="100"
                        onChange={this.handle("size")}
                        value={this.state.size}
                    />
                    <div className="divider"/>
                    <Button className="button" onClick={() => this.naiveBubbleSort()}>Naive Bubble Sort</Button>
                    <div className="divider"/>
                    <Button className="button" onClick={() => this.bubbleSort()}>Bubble Sort</Button>
                    <div className="divider"/>
                    <Button className="button" onClick={() => this.selectionSort()}>Selection Sort</Button>
                    <div className="divider"/>
                    <Button className="button" onClick={() => this.mergeSort()}>Merge Sort</Button>
                    <div className="divider"/>
                    <Button className="button" onClick={() => this.insertionSort()}>Insertion Sort</Button>
                </Nav>
                <Button variant="outline-light" onClick={()=> this.resetArray()}>Reset Array</Button>
            </Navbar>

            <div className="array-container">
              {
                  this.state.array.map((value, key) => (
                      <div key={key} className="array-bar"
                           style={{
                               height: `${value}px`, 
                               width: `${this.state.width}px`,
                               margin: `0 ${this.state.margin}px`
                        }}>
                      </div>
                  )

                )}
            </div>
            </>
        )
    }
}

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}
function areArraysEqual(a1, a2) {
    if (a1.length !== a2.length) return false
    for (let i = 0; i < a1.length; i++) if (a1[i] !== a2[i]) return false
    return true
}
